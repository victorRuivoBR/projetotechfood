package PacoteBean;

public class PedidosBean {

    private String Nome_pedido;
    private String Ingrediente_Pedido;
    private String Preco_Pedido;

    public String getNome_pedido() {
        return Nome_pedido;
    }

    public void setNome_pedido(String Nome_pedido) {
        this.Nome_pedido = Nome_pedido;
    }

    public String getIngrediente_Pedido() {
        return Ingrediente_Pedido;
    }

    public void setIngrediente_Pedido(String Ingrediente_Pedido) {
        this.Ingrediente_Pedido = Ingrediente_Pedido;
    }

    public String getPreco_Pedido() {
        return Preco_Pedido;
    }

    public void setPreco_Pedido(String Preco_Pedido) {
        this.Preco_Pedido = Preco_Pedido;
    }

}
