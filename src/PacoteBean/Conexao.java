package PacoteBean;

import java.sql.*;

public class Conexao {

    public static Connection Abrirconexao() {

        Connection con = null;

        try {
            String url = "";
            url += "jdbc:mysql://127.0.0.1/DBTechFood?";
            url += "user=root&password=root";

            con = DriverManager.getConnection(url);
            System.out.println("Conexão aberta. ");

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return con;
    }

    public static void fecharConexao(Connection con) {

        try {
            con.close();

            System.out.println("Conexão fechada");

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        } catch (Exception e) {

        }
    }

}
