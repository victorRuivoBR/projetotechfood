package PacoteBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CadastroDao {

    private Connection con;

    public CadastroDao(Connection con) {
        setCon(con);
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String adicionar(CadastroBean usuario) {
        String sql = "insert into Cadastro ( id_func, nome, senha, confirmar) values (?,?,?,?)";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getId_Func());
            ps.setString(2, usuario.getNome());

            ps.setString(3, usuario.getSenha());
            ps.setString(4, usuario.getConfirmar());

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }

        } catch (SQLException e) {
            return e.getMessage();
        }

    }

    public String alterar(CadastroBean usuario) {
        String sql = "update Cadastro set  nome = ?,  senha = ? , confirmar = ?";

        sql += "where id_func = ?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getSenha());
            ps.setString(3, usuario.getConfirmar());
            ps.setString(4, usuario.getId_Func());

            if (ps.executeUpdate() > 0) {
                return "Alterado com sucesso";

            } else {
                return "Erro ao alterar";
            }
        } catch (SQLException e) {

            return e.getMessage();
        }
    }

    public List<CadastroBean> listarTodos() {
        String sql = "select * from Cadastro";

        List<CadastroBean> listaUsuario = new ArrayList<CadastroBean>();

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    CadastroBean cb = new CadastroBean();

                    cb.setId_Func(rs.getString(1));
                    cb.setNome(rs.getString(2));

                    cb.setSenha(rs.getString(3));
                    cb.setConfirmar(rs.getString(4));
                    listaUsuario.add(cb);
                }
                return listaUsuario;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public String excluir(CadastroBean usuario) {
        String sql = "delete from Cadastro where Id_Func = ?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getId_Func());

            if (ps.executeUpdate() > 0) {

                return "Excluido com sucesso";

            } else {

                return "Erro ao excluir";

            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

}
