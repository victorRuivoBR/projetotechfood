/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacoteBean;

/**
 *
 * @author LAB_ETECIA
 */
public class BebidasBean {

    private String Cod_Bebida;
    private String Nome_bebida;
    private String Preco_bebida;

    public String getCod_Bebida() {
        return Cod_Bebida;
    }

    public void setCod_Bebida(String Cod_Bebida) {
        this.Cod_Bebida = Cod_Bebida;
    }

    public String getNome_bebida() {
        return Nome_bebida;
    }

    public void setNome_bebida(String Nome_bebida) {
        this.Nome_bebida = Nome_bebida;
    }

    public String getPreco_bebida() {
        return Preco_bebida;
    }

    public void setPreco_bebida(String Preco_bebida) {
        this.Preco_bebida = Preco_bebida;
    }

}
