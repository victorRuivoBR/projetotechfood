package PacoteBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PratosDao {

    private Connection con;

    public PratosDao(Connection con) {
        setCon(con);
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String adicionar(PratosBean usuario) {
        String sql = "insert into Pratos (nome_prato,ingredientes_prato,preco_prato) values (?,?,?)";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, usuario.getNome_Prato());
            ps.setString(2, usuario.getIngredientes_Prato());
            ps.setString(3, usuario.getPreco_Prato());
            ;

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public String adicionarPedido(PratosBean usuario) {
        String sql = "insert into Pedidos (Cod_Prato, nome_prato, Cod_Mesa, preco, quantidade, descricao) values (?,?,?,?,?,?);";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, usuario.getCod_Prato());
            ps.setString(2, usuario.getNome_Prato());
            ps.setString(3, usuario.getIngredientes_Prato());
            ps.setString(4, usuario.getPreco_Prato());
            ;

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public List<PratosBean> listarTodos() {
        String sql = "select * from Pratos";

        List<PratosBean> listaUsuario = new ArrayList<PratosBean>();

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    PratosBean cb = new PratosBean();
                    cb.setCod_Prato(rs.getString(1));
                    cb.setNome_Prato(rs.getString(2));
                    cb.setIngredientes_Prato(rs.getString(3));
                    cb.setPreco_Prato(rs.getString(4));
                    listaUsuario.add(cb);
                }
                return listaUsuario;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public String alterar(PratosBean usuario) {
        String sql = "update Pratos set Nome_Prato=?, Ingredientes_Prato=?, Preco_Prato=?";
        sql += "where Cod_Prato=?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getNome_Prato());
            ps.setString(2, usuario.getIngredientes_Prato());
            ps.setString(3, usuario.getPreco_Prato());
            ps.setString(4, usuario.getCod_Prato());

            if (ps.executeUpdate() > 0) {
                return "Alterado com sucesso";

            } else {
                System.out.println(usuario);
                return "Erro ao alterar";
            }
        } catch (SQLException e) {

            return e.getMessage();
        }

    }

    public String excluir(PratosBean usuario) {
        String sql = "delete from Pratos where Cod_Prato=?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getCod_Prato());

            if (ps.executeUpdate() > 0) {

                return "Excluido com sucesso";

            } else {

                return "Erro ao excluir";

            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }
}
