package PacoteBean;

public class MesaBean {

    private String Cod_Mesa;
    private String nome_pedido;
    private String ingrediente_pedido;
    private String preco_pedido;
    private String confirmar;

    public String getConfirmar() {
        return confirmar;
    }

    public void setConfirmar(String preco_conta) {
        this.confirmar = confirmar;
    }

    public String getCod_Mesa() {
        return Cod_Mesa;
    }

    public void setCod_Mesa(String Cod_Mesa) {
        this.Cod_Mesa = Cod_Mesa;
    }

    public String getNome_pedido() {
        return nome_pedido;
    }

    public void setNome_pedido(String nome_pedido) {
        this.nome_pedido = nome_pedido;
    }

    public String getIngrediente_pedido() {
        return ingrediente_pedido;
    }

    public void setIngrediente_pedido(String ingrediente_pedido) {
        this.ingrediente_pedido = ingrediente_pedido;
    }

    public String getPreco_pedido() {
        return preco_pedido;
    }

    public void setPreco_pedido(String preco_pedido) {
        this.preco_pedido = preco_pedido;
    }

}
