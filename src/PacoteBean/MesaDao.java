package PacoteBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MesaDao {

    private Connection con;

    public MesaDao(Connection con) {
        setCon(con);
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public List<MesaBean> listarTodos() {
        String sql = "select * from Mesa";

        List<MesaBean> listaUsuario = new ArrayList<MesaBean>();

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    MesaBean mb = new MesaBean();
                    mb.setCod_Mesa(rs.getString(1));
                    mb.setNome_pedido(rs.getString(2));
                    mb.setIngrediente_pedido(rs.getString(3));
                    mb.setPreco_pedido(rs.getString(5));

                    listaUsuario.add(mb);
                }
                return listaUsuario;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public String adicionar(MesaBean usuario) {

        String sql = "insert into Mesa (cod_mesa, nome_pedido, ingrediente_pedido, preco_pedido) values (?,?,?,?)";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, usuario.getCod_Mesa());
            ps.setString(2, usuario.getNome_pedido());
            ps.setString(3, usuario.getIngrediente_pedido());
            ps.setString(4, usuario.getPreco_pedido());

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public List<MesaBean> listar(MesaBean usuario) {
        String sql = "select * from Mesa where Cod_Mesa = ?";

        List<MesaBean> listarUsuario = new ArrayList<MesaBean>();

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, usuario.getCod_Mesa());
            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    MesaBean mb = new MesaBean();
                    mb.setCod_Mesa(rs.getString(1));
                    mb.setNome_pedido(rs.getString(2));
                    mb.setPreco_pedido(rs.getString(4));
                    listarUsuario.add(mb);
                }
                return listarUsuario;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public String excluir(MesaBean usuario) {
        String sql = "delete from Mesa where Cod_Mesa = ?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getCod_Mesa());

            if (ps.executeUpdate() > 0) {

                return "Excluido com sucesso";

            } else {

                return "Erro ao excluir";

            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public String alterar(MesaBean usuario) {
        String sql = "update Mesa set confirmar=? where cod_mesa=? && nome_pedido like ? ";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, "Pronto");
            ps.setString(2, usuario.getCod_Mesa());
            ps.setString(3, usuario.getNome_pedido());

            if (ps.executeUpdate() > 0) {
                return "uau";
            } else {
                return "Erro ao inserir.";
            }

        } catch (SQLException e) {
            return e.getMessage();
        }

    }
}
