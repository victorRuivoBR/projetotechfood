/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacoteBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LAB_ETECIA
 */
public class BebidasDao {

    private Connection con;

    public BebidasDao(Connection con) {
        setCon(con);
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String adicionar(BebidasBean usuario) {
        String sql = "insert into Bebidas (cod_bebida, nome_bebida, preco_bebida) values (?,?,?)";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ps.setString(1, usuario.getCod_Bebida());
            ps.setString(2, usuario.getNome_bebida());
            ps.setString(3, usuario.getPreco_bebida());

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }

        } catch (SQLException e) {
            return e.getMessage();
        }

    }

    public String alterar(BebidasBean usuario) {
        String sql = "update Bebidas set nome_Bebida=?, preco_Bebida=?";
        sql += "where cod_bebida=?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getNome_bebida());
            ps.setString(2, usuario.getPreco_bebida());
            ps.setString(3, usuario.getCod_Bebida());

            if (ps.executeUpdate() > 0) {
                return "Alterado com sucesso";

            } else {
                return "Erro ao alterar";
            }
        } catch (SQLException e) {

            return e.getMessage();
        }
    }

    public List<BebidasBean> listarTodos() {
        String sql = "select * from Bebidas";

        List<BebidasBean> listaUsuario = new ArrayList<BebidasBean>();

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            if (rs != null) {

                while (rs.next()) {
                    BebidasBean bb = new BebidasBean();
                    bb.setCod_Bebida(rs.getString(1));
                    bb.setNome_bebida(rs.getString(2));
                    bb.setPreco_bebida(rs.getString(3));
                    listaUsuario.add(bb);
                }
                return listaUsuario;
            } else {
                return null;
            }
        } catch (SQLException e) {
            return null;
        }
    }

    public String excluir(BebidasBean usuario) {
        String sql = "delete from Bebidas where Cod_Bebida = ?";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getCod_Bebida());

            if (ps.executeUpdate() > 0) {

                return "Excluido com sucesso";

            } else {

                return "Erro ao excluir";

            }
        } catch (SQLException e) {
            return e.getMessage();
        }
    }
}
