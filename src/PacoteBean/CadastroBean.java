package PacoteBean;

public class CadastroBean {

    private String Id_Func;
    private String nome;
    private String senha;
    private String confirmar;

    public String getId_Func() {
        return Id_Func;
    }

    public void setId_Func(String Id_Func) {
        this.Id_Func = Id_Func;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfirmar() {
        return confirmar;
    }

    public void setConfirmar(String confirmar) {
        this.confirmar = confirmar;
    }

}
