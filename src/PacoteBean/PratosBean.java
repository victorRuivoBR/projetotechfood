package PacoteBean;

public class PratosBean {

    private String Cod_Prato;
    private String Nome_Prato;
    private String Ingredientes_Prato;
    private String Preco_Prato;

    public String getCod_Prato() {
        return Cod_Prato;
    }

    public void setCod_Prato(String Cod_Prato) {
        this.Cod_Prato = Cod_Prato;
    }

    public String getNome_Prato() {
        return Nome_Prato;
    }

    public void setNome_Prato(String Nome_Prato) {
        this.Nome_Prato = Nome_Prato;
    }

    public String getIngredientes_Prato() {
        return Ingredientes_Prato;
    }

    public void setIngredientes_Prato(String Ingredientes_Prato) {
        this.Ingredientes_Prato = Ingredientes_Prato;
    }

    public String getPreco_Prato() {
        return Preco_Prato;
    }

    public void setPreco_Prato(String Preco_Prato) {
        this.Preco_Prato = Preco_Prato;
    }

}
