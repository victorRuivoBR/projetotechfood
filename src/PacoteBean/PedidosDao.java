package PacoteBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PedidosDao {

    private Connection con;

    public PedidosDao(Connection con) {
        setCon(con);
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public String adicionar(PedidosBean usuario) {
        String sql = "insert into Pedidos (Nome_pedido, Ingrediente_Pedido, Preco_Pedido) values (?,?,?)";

        try {
            PreparedStatement ps = getCon().prepareStatement(sql);

            ps.setString(1, usuario.getNome_pedido());
            ps.setString(2, usuario.getIngrediente_Pedido());
            ps.setString(3, usuario.getPreco_Pedido());

            if (ps.executeUpdate() > 0) {
                return "Inserido com sucesso.";
            } else {
                return "Erro ao inserir.";
            }

        } catch (SQLException e) {
            return e.getMessage();
        }

    }

}
