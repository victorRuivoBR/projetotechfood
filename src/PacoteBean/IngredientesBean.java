package PacoteBean;

public class IngredientesBean {

    private String nome_ingr;
    private String Cod_Ingr;

    public String getNome_ingr() {
        return nome_ingr;
    }

    public void setNome_ingr(String nome_ingr) {
        this.nome_ingr = nome_ingr;
    }

    public String getCod_Ingr() {
        return Cod_Ingr;
    }

    public void setCod_Ingr(String Cod_Ingr) {
        this.Cod_Ingr = Cod_Ingr;
    }

}
