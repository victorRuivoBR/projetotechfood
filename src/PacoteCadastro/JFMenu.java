/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacoteCadastro;

/**
 *
 * @author Juan
 */
public class JFMenu extends javax.swing.JFrame {

    /**
     * Creates new form JFMenu1
     */
    public JFMenu() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollBar1 = new javax.swing.JScrollBar();
        jPanel1 = new javax.swing.JPanel();
        btnCaixa = new javax.swing.JButton();
        btnCozinha = new javax.swing.JButton();
        btnPedidos = new javax.swing.JButton();
        btnGerenciarUsu = new javax.swing.JButton();
        btnGerenciarPedi = new javax.swing.JButton();
        btnMesas = new javax.swing.JButton();
        btnVoltar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Menu");
        setPreferredSize(new java.awt.Dimension(775, 490));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(196, 0, 0));
        jPanel1.setPreferredSize(new java.awt.Dimension(775, 401));
        jPanel1.setLayout(null);

        btnCaixa.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnCaixa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/money.png"))); // NOI18N
        btnCaixa.setText("Caixa");
        btnCaixa.setToolTipText("Caixa");
        btnCaixa.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCaixa.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCaixa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaixaActionPerformed(evt);
            }
        });
        jPanel1.add(btnCaixa);
        btnCaixa.setBounds(90, 150, 200, 100);

        btnCozinha.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnCozinha.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/chef.png"))); // NOI18N
        btnCozinha.setText("Cozinha");
        btnCozinha.setToolTipText("Cozinha");
        btnCozinha.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCozinha.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCozinha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCozinhaActionPerformed(evt);
            }
        });
        jPanel1.add(btnCozinha);
        btnCozinha.setBounds(310, 150, 169, 100);

        btnPedidos.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnPedidos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/dinner-tray-with-cover.png"))); // NOI18N
        btnPedidos.setText("Pedidos");
        btnPedidos.setToolTipText("Pedidos");
        btnPedidos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPedidos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPedidosActionPerformed(evt);
            }
        });
        jPanel1.add(btnPedidos);
        btnPedidos.setBounds(490, 150, 169, 100);

        btnGerenciarUsu.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnGerenciarUsu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/user.png"))); // NOI18N
        btnGerenciarUsu.setText("Gerenciar Usuários");
        btnGerenciarUsu.setToolTipText("Gerenciamento de Usuários");
        btnGerenciarUsu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGerenciarUsu.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGerenciarUsu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerenciarUsuActionPerformed(evt);
            }
        });
        jPanel1.add(btnGerenciarUsu);
        btnGerenciarUsu.setBounds(90, 270, 204, 100);

        btnGerenciarPedi.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnGerenciarPedi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/knife-fork-and-plate.png"))); // NOI18N
        btnGerenciarPedi.setText("Gerenciar Pratos");
        btnGerenciarPedi.setToolTipText("Gerenciamento de Pedidos");
        btnGerenciarPedi.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGerenciarPedi.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGerenciarPedi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerenciarPediActionPerformed(evt);
            }
        });
        jPanel1.add(btnGerenciarPedi);
        btnGerenciarPedi.setBounds(310, 270, 169, 100);

        btnMesas.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnMesas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/glass (1).png"))); // NOI18N
        btnMesas.setText("Gerenciar Bebida");
        btnMesas.setToolTipText("Mesas");
        btnMesas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnMesas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnMesas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMesasActionPerformed(evt);
            }
        });
        jPanel1.add(btnMesas);
        btnMesas.setBounds(490, 270, 169, 100);

        btnVoltar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });
        jPanel1.add(btnVoltar);
        btnVoltar.setBounds(650, 420, 72, 32);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/Background 1.png"))); // NOI18N
        jPanel1.add(jLabel2);
        jLabel2.setBounds(-6, 0, 780, 470);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGerenciarPediActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerenciarPediActionPerformed
        JFGerenciarPratos abrir = new JFGerenciarPratos();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnGerenciarPediActionPerformed

    private void btnCaixaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaixaActionPerformed
        JFCaixa abrir = new JFCaixa();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnCaixaActionPerformed

    private void btnCozinhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCozinhaActionPerformed
        JFCozinha abrir = new JFCozinha();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnCozinhaActionPerformed

    private void btnPedidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPedidosActionPerformed
        JFPedidos abrir = new JFPedidos();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnPedidosActionPerformed

    private void btnGerenciarUsuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerenciarUsuActionPerformed
        JFGerenciarUsuario abrir = new JFGerenciarUsuario();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnGerenciarUsuActionPerformed

    private void btnMesasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMesasActionPerformed
        JFGerenciarBebidas abrir = new JFGerenciarBebidas();
        abrir.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_btnMesasActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        JFLogin abrir = new JFLogin();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnVoltarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCaixa;
    private javax.swing.JButton btnCozinha;
    private javax.swing.JButton btnGerenciarPedi;
    private javax.swing.JButton btnGerenciarUsu;
    private javax.swing.JButton btnMesas;
    private javax.swing.JButton btnPedidos;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollBar jScrollBar1;
    // End of variables declaration//GEN-END:variables
}
