package PacoteCadastro;

import PacoteBean.BebidasBean;
import PacoteBean.BebidasDao;
import PacoteBean.Conexao;
import PacoteBean.PratosBean;
import PacoteBean.PratosDao;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author renan
 */
public class JFGerenciarBebidas extends javax.swing.JFrame {

    public JFGerenciarBebidas() {
        initComponents();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblGerenciarBebidas = new javax.swing.JLabel();
        lblNome = new javax.swing.JLabel();
        lblPreco = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtPreco = new javax.swing.JTextField();
        btnCadastrar = new javax.swing.JButton();
        btnAlterar = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        btnVoltar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbBebidas = new javax.swing.JTable();
        lblMensagem = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        lblCod = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Gerenciamento");
        setPreferredSize(new java.awt.Dimension(775, 520));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(196, 0, 0));
        jPanel1.setLayout(null);

        lblGerenciarBebidas.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        lblGerenciarBebidas.setForeground(new java.awt.Color(255, 255, 255));
        lblGerenciarBebidas.setText("Gerenciamento de Bebidas");
        jPanel1.add(lblGerenciarBebidas);
        lblGerenciarBebidas.setBounds(280, 30, 400, 44);

        lblNome.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblNome.setForeground(new java.awt.Color(255, 255, 255));
        lblNome.setText("Nome:");
        jPanel1.add(lblNome);
        lblNome.setBounds(220, 170, 51, 23);

        lblPreco.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblPreco.setForeground(new java.awt.Color(255, 255, 255));
        lblPreco.setText("Preço:");
        jPanel1.add(lblPreco);
        lblPreco.setBounds(220, 220, 48, 23);

        txtNome.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jPanel1.add(txtNome);
        txtNome.setBounds(280, 170, 201, 29);

        txtPreco.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        jPanel1.add(txtPreco);
        txtPreco.setBounds(280, 210, 201, 29);

        btnCadastrar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnCadastrar.setText("Cadastrar");
        btnCadastrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCadastrarMouseClicked(evt);
            }
        });
        btnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCadastrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCadastrar);
        btnCadastrar.setBounds(40, 280, 120, 31);

        btnAlterar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnAlterar.setText("Alterar");
        btnAlterar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAlterarMouseClicked(evt);
            }
        });
        btnAlterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlterarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAlterar);
        btnAlterar.setBounds(340, 280, 100, 31);

        btnLimpar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnLimpar.setText("Limpar");
        btnLimpar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLimparMouseClicked(evt);
            }
        });
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });
        jPanel1.add(btnLimpar);
        btnLimpar.setBounds(480, 280, 100, 31);

        btnExcluir.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnExcluir.setText("Excluir");
        btnExcluir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExcluirMouseClicked(evt);
            }
        });
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });
        jPanel1.add(btnExcluir);
        btnExcluir.setBounds(620, 280, 90, 31);

        btnVoltar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnVoltar.setText("Voltar");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });
        jPanel1.add(btnVoltar);
        btnVoltar.setBounds(620, 440, 91, 29);

        btnPesquisar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnPesquisar.setText("Pesquisar");
        btnPesquisar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPesquisarMouseClicked(evt);
            }
        });
        jPanel1.add(btnPesquisar);
        btnPesquisar.setBounds(200, 280, 110, 31);

        tbBebidas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Código", "Nome", "Preço"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbBebidas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbBebidasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbBebidas);
        if (tbBebidas.getColumnModel().getColumnCount() > 0) {
            tbBebidas.getColumnModel().getColumn(0).setResizable(false);
            tbBebidas.getColumnModel().getColumn(1).setResizable(false);
            tbBebidas.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 329, 740, 92);

        lblMensagem.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblMensagem.setForeground(new java.awt.Color(255, 255, 255));
        lblMensagem.setText("Resultado:");
        jPanel1.add(lblMensagem);
        lblMensagem.setBounds(100, 440, 360, 23);

        lblCodigo.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblCodigo.setForeground(new java.awt.Color(255, 255, 255));
        lblCodigo.setText("Código:");
        jPanel1.add(lblCodigo);
        lblCodigo.setBounds(220, 130, 58, 23);

        lblCod.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblCod.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(lblCod);
        lblCod.setBounds(290, 140, 34, 14);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/Background 1.png"))); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, -10, 770, 500);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 768, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 479, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCadastrarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCadastrarActionPerformed

    private void btnAlterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlterarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAlterarActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLimparActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        JFMenu abrir = new JFMenu();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnVoltarActionPerformed

    private void btnCadastrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCadastrarMouseClicked
        Connection con = Conexao.Abrirconexao();

        BebidasBean bb = new BebidasBean();
        BebidasDao bd = new BebidasDao(con);

        bb.setNome_bebida(txtNome.getText());
        bb.setPreco_bebida(txtPreco.getText());

        lblMensagem.setText("Resultado: " + bd.adicionar(bb));

        Conexao.fecharConexao(con);        // TODO add your handling code here:
    }//GEN-LAST:event_btnCadastrarMouseClicked

    private void btnPesquisarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPesquisarMouseClicked
        Connection con = Conexao.Abrirconexao();

        BebidasDao bd = new BebidasDao(con);
        List<BebidasBean> listaCarro = new ArrayList<BebidasBean>();
        listaCarro = bd.listarTodos();

        DefaultTableModel tbm = (DefaultTableModel) tbBebidas.getModel();

        for (int i = tbm.getRowCount() - 1; i >= 0; i--) {
            tbm.removeRow(i);

        }
        int i = 0;

        for (BebidasBean bb : listaCarro) {
            tbm.addRow(new String[1]);
            tbBebidas.setValueAt(bb.getCod_Bebida(), i, 0);
            tbBebidas.setValueAt(bb.getNome_bebida(), i, 1);
            tbBebidas.setValueAt(bb.getPreco_bebida(), i, 2);
            i++;

        }
        Conexao.fecharConexao(con);
    }//GEN-LAST:event_btnPesquisarMouseClicked

    private void tbBebidasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbBebidasMouseClicked
        Integer linha = tbBebidas.getSelectedRow();
        String Cod = (String) tbBebidas.getValueAt(linha, 0);
        String Nome = (String) tbBebidas.getValueAt(linha, 1);
        String Preco = (String) tbBebidas.getValueAt(linha, 2);
        lblCod.setText(Cod);
        txtNome.setText(Nome);
        txtPreco.setText(Preco);
    }//GEN-LAST:event_tbBebidasMouseClicked

    private void btnAlterarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAlterarMouseClicked
        Connection con = Conexao.Abrirconexao();

        BebidasBean bb = new BebidasBean();
        BebidasDao bd = new BebidasDao(con);

        bb.setCod_Bebida(lblCod.getText());
        bb.setNome_bebida(txtNome.getText());
        bb.setPreco_bebida(txtPreco.getText());

        lblMensagem.setText("Resultado: " + bd.alterar(bb));

        Conexao.fecharConexao(con);        // TODO add your handling code here:
    }//GEN-LAST:event_btnAlterarMouseClicked

    private void btnExcluirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExcluirMouseClicked
        Connection con = Conexao.Abrirconexao();

        BebidasBean bb = new BebidasBean();
        BebidasDao bd = new BebidasDao(con);

        bb.setCod_Bebida(lblCod.getText());

        Object[] opcoes = {"Sim", "Nao"};

        int i = JOptionPane.showOptionDialog(null, "Deseja excluir essa Bebida : " + txtNome.getText() + "?", "Exclusão", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opcoes, opcoes[0]);
        if (i == JOptionPane.YES_OPTION) {
            lblMensagem.setText("Resultado: " + bd.excluir(bb));
        }
        Conexao.fecharConexao(con);        // TODO add your handling code here:
    }//GEN-LAST:event_btnExcluirMouseClicked

    private void btnLimparMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLimparMouseClicked
        lblCod.setText("");
        txtNome.setText("");
        txtPreco.setText("");

        lblMensagem.setText("");

        DefaultTableModel tbm = (DefaultTableModel) tbBebidas.getModel();

        for (int i = tbm.getRowCount() - 1; i >= 0; i--) {
            tbm.removeRow(i);
        }
    }//GEN-LAST:event_btnLimparMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFGerenciarBebidas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAlterar;
    private javax.swing.JButton btnCadastrar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCod;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblGerenciarBebidas;
    private javax.swing.JLabel lblMensagem;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPreco;
    private javax.swing.JTable tbBebidas;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPreco;
    // End of variables declaration//GEN-END:variables

}
