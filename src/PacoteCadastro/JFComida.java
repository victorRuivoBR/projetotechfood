package PacoteCadastro;

import PacoteBean.Conexao;
import PacoteBean.PedidosBean;
import PacoteBean.PedidosDao;
import PacoteBean.PratosBean;
import PacoteBean.PratosDao;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LAB_ETECIA
 */
public class JFComida extends javax.swing.JDialog {

    DefaultTableModel tbm;
    ArrayList<PratosBean> lista;

    public JFComida() {
        initComponents();
        setModal(true);
        lista = new ArrayList<PratosBean>();
    }

    public ArrayList<PratosBean> getSelecionados() {
        return lista;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblComida = new javax.swing.JLabel();
        btnConfirmar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbComida = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();

        setTitle("Comidas");
        setPreferredSize(new java.awt.Dimension(775, 515));

        jPanel1.setBackground(new java.awt.Color(196, 0, 0));
        jPanel1.setLayout(null);

        lblComida.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        lblComida.setForeground(new java.awt.Color(255, 255, 255));
        lblComida.setText("Comidas");
        jPanel1.add(lblComida);
        lblComida.setBounds(303, 56, 127, 44);

        btnConfirmar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnConfirmar.setText("Confirmar");
        btnConfirmar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnConfirmarMouseClicked(evt);
            }
        });
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });
        jPanel1.add(btnConfirmar);
        btnConfirmar.setBounds(250, 420, 109, 31);

        btnCancelar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);
        btnCancelar.setBounds(440, 420, 110, 31);

        tbComida.setBackground(new java.awt.Color(215, 215, 215));
        tbComida.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        tbComida.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Selecionar", "Prato", "Ingredientes", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbComida.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbComidaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbComida);
        if (tbComida.getColumnModel().getColumnCount() > 0) {
            tbComida.getColumnModel().getColumn(0).setResizable(false);
            tbComida.getColumnModel().getColumn(1).setResizable(false);
            tbComida.getColumnModel().getColumn(2).setResizable(false);
            tbComida.getColumnModel().getColumn(3).setResizable(false);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 170, 750, 190);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/Background 1.png"))); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, -40, 780, 560);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 775, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente sair?", "TechFood BETA", JOptionPane.YES_NO_OPTION);
        if (resposta == 0) {
            this.setVisible(false);
        }      // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed
    public void add() {

        Connection con = Conexao.Abrirconexao();

        PedidosBean pb = new PedidosBean();
        PedidosDao pd = new PedidosDao(con);

        Conexao.fecharConexao(con);

    }
    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        for (int i = 0; i < tbm.getRowCount(); i++) {
            if (tbComida.getValueAt(i, 0) != null) {
                PratosBean p = new PratosBean();
                p.setNome_Prato((String) tbComida.getValueAt(i, 1));
                p.setIngredientes_Prato((String) tbComida.getValueAt(i, 2));
                p.setPreco_Prato((String) tbComida.getValueAt(i, 3));
                lista.add(p);
            }
        }

        setVisible(false);

        //add();
        //JFPedidos abrir = new JFPedidos();
        //abrir.setVisible(true);
        //this.setVisible(false);
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void tbComidaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbComidaMouseClicked
        /* Integer linha = tbComida.getSelectedRow();
        String prato = (String) tbComida.getValueAt(linha, 1);
        String ingrediente = (String) tbComida.getValueAt(linha, 2);
        String preco = (String) tbComida.getValueAt(linha, 3);
        System.out.println(prato);
         */

    }//GEN-LAST:event_tbComidaMouseClicked
    public void pesquisar() {
        Connection con = Conexao.Abrirconexao();

        PratosDao cd = new PratosDao(con);
        List<PratosBean> listaCarro = new ArrayList<PratosBean>();
        listaCarro = cd.listarTodos();

        tbm = (DefaultTableModel) tbComida.getModel();

        for (int i = tbm.getRowCount() - 1; i >= 0; i--) {
            tbm.removeRow(i);

        }
        int i = 0;

        for (PratosBean cb : listaCarro) {
            tbm.addRow(new String[1]);

            tbComida.setValueAt(cb.getNome_Prato(), i, 1);

            tbComida.setValueAt(cb.getIngredientes_Prato(), i, 2);

            tbComida.setValueAt(cb.getPreco_Prato(), i, 3);

            i++;

        }
        Conexao.fecharConexao(con);

    }

    private void btnConfirmarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnConfirmarMouseClicked
        /*add();

        JFPedidos abrir = new JFPedidos();
        abrir.setVisible(true);
        this.setVisible(false);
         */

    }//GEN-LAST:event_btnConfirmarMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFComida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFComida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFComida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFComida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFComida().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblComida;
    private javax.swing.JTable tbComida;
    // End of variables declaration//GEN-END:variables
}
