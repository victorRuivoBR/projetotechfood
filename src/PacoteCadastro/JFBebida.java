/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PacoteCadastro;

import PacoteBean.BebidasBean;
import PacoteBean.BebidasDao;
import PacoteBean.Conexao;
import PacoteBean.PedidosBean;
import PacoteBean.PedidosDao;
import PacoteBean.PratosBean;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LAB_ETECIA
 */
public class JFBebida extends javax.swing.JDialog {

    DefaultTableModel tbm;
    ArrayList<BebidasBean> lista;

    public JFBebida() {
        initComponents();
        setModal(true);
        lista = new ArrayList<BebidasBean>();

    }

    public ArrayList<BebidasBean> getSelecionados() {
        return lista;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbBebidas = new javax.swing.JTable();
        btnConfirmar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblBebidas = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Bebidas");

        jPanel1.setBackground(new java.awt.Color(196, 0, 0));
        jPanel1.setLayout(null);

        tbBebidas.setBackground(new java.awt.Color(215, 215, 215));
        tbBebidas.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        tbBebidas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Selecionar", "Bebida", "Preço"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tbBebidas);
        if (tbBebidas.getColumnModel().getColumnCount() > 0) {
            tbBebidas.getColumnModel().getColumn(0).setResizable(false);
            tbBebidas.getColumnModel().getColumn(1).setResizable(false);
            tbBebidas.getColumnModel().getColumn(2).setResizable(false);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 185, 750, 94);

        btnConfirmar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });
        jPanel1.add(btnConfirmar);
        btnConfirmar.setBounds(230, 388, 109, 31);

        btnCancelar.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);
        btnCancelar.setBounds(407, 388, 97, 31);

        lblBebidas.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        lblBebidas.setForeground(new java.awt.Color(255, 255, 255));
        lblBebidas.setText("Bebidas");
        jPanel1.add(lblBebidas);
        lblBebidas.setBounds(323, 43, 117, 44);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PacoteTechFood/Icones/Background 1.png"))); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(-6, 0, 780, 470);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 775, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente sair?", "TechFood BETA", JOptionPane.YES_NO_OPTION);
        if (resposta == 0) {
            this.setVisible(false);
        }      // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed
    public void add() {

        Connection con = Conexao.Abrirconexao();

        PedidosBean pb = new PedidosBean();
        PedidosDao pd = new PedidosDao(con);

        Conexao.fecharConexao(con);

    }
    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed

        for (int i = 0; i < tbm.getRowCount(); i++) {
            if (tbBebidas.getValueAt(i, 0) != null) {
                BebidasBean p = new BebidasBean();
                p.setNome_bebida((String) tbBebidas.getValueAt(i, 1));
                p.setPreco_bebida((String) tbBebidas.getValueAt(i, 2));
                lista.add(p);
            }
        }

        setVisible(false);

        //add();
        //JFPedidos abrir = new JFPedidos();
        //abrir.setVisible(true);
        //this.setVisible(false);
    }//GEN-LAST:event_btnConfirmarActionPerformed

    public void pesquisarBebida() {
        Connection con = Conexao.Abrirconexao();

        BebidasDao cd = new BebidasDao(con);
        List<BebidasBean> listaCarro = new ArrayList<BebidasBean>();
        listaCarro = cd.listarTodos();

        tbm = (DefaultTableModel) tbBebidas.getModel();

        for (int i = tbm.getRowCount() - 1; i >= 0; i--) {
            tbm.removeRow(i);

        }
        int i = 0;

        for (BebidasBean cb : listaCarro) {
            tbm.addRow(new String[1]);

            tbBebidas.setValueAt(cb.getNome_bebida(), i, 1);

            tbBebidas.setValueAt(cb.getPreco_bebida(), i, 2);

            i++;

        }
        Conexao.fecharConexao(con);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFBebida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFBebida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFBebida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFBebida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFBebida().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBebidas;
    private javax.swing.JTable tbBebidas;
    // End of variables declaration//GEN-END:variables
}
